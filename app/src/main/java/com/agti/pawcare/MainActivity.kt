package com.agti.pawcare

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.agti.pawcare.databinding.ActivityMainBinding
import com.agti.pawcare.utility.SharedPrefManager
import com.agti.pawcare.utility.toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity(), FirebaseAuth.AuthStateListener {
    val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val navController by lazy { this.findNavController(R.id.nav_host_fragment_container) }
//    private val sharedViewModel: SharedViewModel by viewModels { Injection.provideSharedViewModelFactory() }
//    private var currentNavController: LiveData<NavController>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SharedPrefManager.init(this)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        binding.bottomNavigation.setupWithNavController(navController)
//        if (savedInstanceState == null) {
//            setupNavController()
//        }
    }

//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//
//        setupNavController()
//    }
//
//    private fun setupNavController() {
//        // TODO: setup NavController
//
//    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            200 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        // TODO: grant location updates
                        navController.navigate(R.id.homeFragment)
                        return
                    }
                }
            }
            else -> {
                this.toast("Izin untuk menggunakan lokasi tidak diberikan")
                return
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() ?: false
    }

    override fun onAuthStateChanged(auth: FirebaseAuth) {
        // TODO: Potentially bug code
        SharedPrefManager.isSignIn = auth.currentUser?.uid
    }
}