package com.agti.pawcare.data.model

data class Item(
        val id: Any? = null,
        val title: String,
        val description: String? = null,
        val image: Any,
        val isVisible: Boolean = true,
        val route: Int? = null
)