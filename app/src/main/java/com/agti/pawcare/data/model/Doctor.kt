package com.agti.pawcare.data.model

import com.agti.pawcare.utility.moneyFormat
import com.google.gson.annotations.SerializedName

data class Doctor(
        @SerializedName("id_dokter") var idDokter: Int,
        @SerializedName("nama_dokter") var namaDokter: String,
        @SerializedName("pengalaman") var pengalaman: Int,
        @SerializedName("tarif") var tarif: Int,
        @SerializedName("tersedia") var tersedia: String,
        @SerializedName("pendidikan") var pendidikan: String,
        @SerializedName("klinik") var klinik: String,
        @SerializedName("no_hp") var noHp: String,
        @SerializedName("foto_dokter") var foto_dokter: String) {
    fun formattedTarif() = tarif.moneyFormat()
}