package com.agti.pawcare.data.source

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.agti.pawcare.data.model.Article
import com.agti.pawcare.data.model.NetworkState

class ArticlePageKeyedDataSource : PageKeyedDataSource<Int, Article>() {
    private val _networkState = MutableLiveData<NetworkState>()
    private val _error = MutableLiveData<String>()

    val networkState: LiveData<NetworkState>
        get() = _networkState

    val error: LiveData<String>
        get() = _error

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Article>) {
        TODO("Not yet implemented")
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        TODO("Not yet implemented")
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        TODO("Not yet implemented")
    }
}