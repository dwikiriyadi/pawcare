package com.agti.pawcare.data.model

import android.os.Bundle
import androidx.lifecycle.LiveData

enum class NetworkState {
    RUNNING, SUCCESS, FAILED
}

data class Result<T>(
        val data: LiveData<T>,
        val error: LiveData<String>,
        val networkState: LiveData<NetworkState>,
        val extras: Bundle? = null
)