package com.agti.pawcare.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Article(
        @SerializedName("id_artikel") var idArtikel: Int,
        @SerializedName("judul_artikel") var judulArtikel: String?,
        @SerializedName("kategori") var kategori: String?,
        @SerializedName("gambar_artikel") var gambarArtikel: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idArtikel)
        parcel.writeString(judulArtikel)
        parcel.writeString(kategori)
        parcel.writeString(gambarArtikel)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Article> {
        override fun createFromParcel(parcel: Parcel): Article {
            return Article(parcel)
        }

        override fun newArray(size: Int): Array<Article?> {
            return arrayOfNulls(size)
        }
    }
}