package com.agti.pawcare.data

import com.agti.pawcare.data.model.Article
import com.agti.pawcare.data.model.Response
import com.agti.pawcare.data.model.User
import retrofit2.Call
import retrofit2.Callback

fun getArticles(page: Int, onSuccess: (response: Response<ArrayList<Article>>) -> Unit, onFail: (error: String) -> Unit) {
    Services.create().getArticles(page.toString()).enqueue(object : Callback<Response<ArrayList<Article>>> {
        override fun onFailure(call: Call<Response<ArrayList<Article>>>, t: Throwable) {
            t.message?.let { onFail(it) }
        }

        override fun onResponse(call: Call<Response<ArrayList<Article>>>, response: retrofit2.Response<Response<ArrayList<Article>>>) {
            if (response.isSuccessful) {
                response.body()?.let { onSuccess(it) }
            } else {
                onFail(response.errorBody().toString())
            }
        }
    })
}

fun getProfile(uid: String, onSuccess: (response: Response<User>?) -> Unit, onFail: (error: String) -> Unit) {
    Services.create().getProfile(uid).enqueue(object : Callback<Response<User>> {
        override fun onFailure(call: Call<Response<User>>, t: Throwable) {
            t.message?.let { onFail(it) }
        }

        override fun onResponse(call: Call<Response<User>>, response: retrofit2.Response<Response<User>>) {
            if (response.isSuccessful) {
                onSuccess(response.body())
            } else {
                onFail(response.code().toString())
            }
        }
    })
}

fun updateProfile(id: String, onSuccess: (response: Response<User>?) -> Unit, onFail: (error: String) -> Unit) {
    Services.create().updateProfile(id).enqueue(object : Callback<Response<User>> {
        override fun onFailure(call: Call<Response<User>>, t: Throwable) {
            t.message?.let { onFail(it) }
        }

        override fun onResponse(call: Call<Response<User>>, response: retrofit2.Response<Response<User>>) {
            if (response.isSuccessful) {
                onSuccess(response.body())
            } else {
                onFail(response.errorBody().toString())
            }
        }

    })
}