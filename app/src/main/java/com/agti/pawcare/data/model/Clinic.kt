package com.agti.pawcare.data.model

import com.google.gson.annotations.SerializedName

class Clinic(
        @SerializedName("id_klinik") var idKlinik: Int,
        @SerializedName("nama_klinik") var namaKlinik: String,
        @SerializedName("alamat_klinik") var alamatKlinik: String,
        @SerializedName("foto") var foto: String)