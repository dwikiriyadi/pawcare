package com.agti.pawcare.data.model

import com.google.gson.annotations.SerializedName

data class User(
        @SerializedName("id_pengguna") val idPengguna: Int,
        @SerializedName("nama_depan") var firstName: String? = null,
        @SerializedName("nama_belakang") var lastName: String? = null,
        @SerializedName("jenis_kelamin") var gender: String? = null,
        @SerializedName("email") var email: String? = null,
        @SerializedName("phone_number") var phoneNumber: String? = null,
        @SerializedName("uid") val uid: String? = null
)