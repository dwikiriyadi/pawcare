package com.agti.pawcare.data.source

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.agti.pawcare.data.model.Article

class ArticleDataSourceFactory : DataSource.Factory<Int, Article>() {

    val source = MutableLiveData<ArticlePageKeyedDataSource>()

    override fun create(): DataSource<Int, Article> {
        val source = ArticlePageKeyedDataSource()
        this.source.value = source
        return source
    }

}