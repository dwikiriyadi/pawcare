package com.agti.pawcare.data.source

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.agti.pawcare.data.model.NetworkState
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit

class FirebaseSource {

    private val _user = MutableLiveData<FirebaseUser>()
    val user: LiveData<FirebaseUser>
        get() = _user

    private val _verificationId = MutableLiveData<String>()
    val verificationId: LiveData<String>
        get() = _verificationId

    private val _resendToken = MutableLiveData<PhoneAuthProvider.ForceResendingToken>()
    val resendToken: LiveData<PhoneAuthProvider.ForceResendingToken>
        get() = _resendToken

    private val _error = MutableLiveData<String>()
    val error: LiveData<String>
        get() = _error

    private val _networkState = MutableLiveData<NetworkState>()
    val networkState: LiveData<NetworkState>
        get() = _networkState

    private val firebaseAuth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    fun requestVerificationCode(executor: Executor, phoneNumber: String, duration: Long) {
        _networkState.value = NetworkState.RUNNING

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber, duration, TimeUnit.SECONDS, executor, object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                _networkState.value = NetworkState.FAILED
                _error.value = e.message
            }

            override fun onCodeSent(verificationId: String, token: PhoneAuthProvider.ForceResendingToken) {
                _verificationId.value = verificationId
                _resendToken.value = token
            }

            override fun onCodeAutoRetrievalTimeOut(verificationId: String) {
                _verificationId.value = verificationId
            }
        }
        )
    }

    fun signOut() = firebaseAuth.signOut()

    fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                _networkState.value = NetworkState.SUCCESS
                _user.value = task.result?.user
            } else {
                _networkState.value = NetworkState.FAILED
                _error.value = task.exception?.message
            }
        }
    }
}