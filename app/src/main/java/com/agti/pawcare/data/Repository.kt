package com.agti.pawcare.data

import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.agti.pawcare.data.model.*
import com.agti.pawcare.data.source.ArticleDataSourceFactory
import com.agti.pawcare.data.source.FirebaseSource
import com.google.firebase.auth.PhoneAuthCredential
import java.util.concurrent.Executor

class Repository(private val executor: Executor) {
    private val _error = MutableLiveData<String>()
    private val _networkState = MutableLiveData<NetworkState>()

    fun signInWithPhoneNumber(phoneNumber: String, duration: Long): Result<Response<User>> {
        val firebaseSource = FirebaseSource()
        firebaseSource.requestVerificationCode(executor, phoneNumber, duration)

        Transformations.map(firebaseSource.error) {
            _error.value = it
        }

        val user = Transformations.switchMap(firebaseSource.user) { firebaseUser ->
            getUser(firebaseUser.uid).data
        }

        Transformations.map(firebaseSource.networkState) {
            _networkState.value = it
        }

        return Result(user, _error, _networkState, bundleOf(Pair("verificationId", firebaseSource.verificationId)))
    }

    fun signInWithCredential(credential: PhoneAuthCredential): Result<Response<User>> {
        val firebaseSource = FirebaseSource()
        firebaseSource.signInWithPhoneAuthCredential(credential)

        Transformations.map(firebaseSource.error) {
            _error.value = it
        }

        val user = Transformations.switchMap(firebaseSource.user) { firebaseUser ->
            getUser(firebaseUser.uid).data
        }

        Transformations.map(firebaseSource.networkState) {
            _networkState.value = it
        }

        return Result(user, _error, _networkState, bundleOf(Pair("verificationId", firebaseSource.verificationId)))
    }

    fun signOut() = FirebaseSource().signOut()

    fun getUser(uid: String): Result<Response<User>> {
        val data = MutableLiveData<Response<User>>()

        _networkState.value = NetworkState.RUNNING

        getProfile(uid, { response ->
            _networkState.value = NetworkState.SUCCESS
            data.value = response
        }, { error ->
            _networkState.value = NetworkState.FAILED
            _error.value = error
        })

        return Result(data, _error, _networkState)
    }

    fun updateUser(uid: String): Result<Response<User>> {
        val data = MutableLiveData<Response<User>>()

        _networkState.value = NetworkState.RUNNING

        updateProfile(uid, { response ->
            _networkState.value = NetworkState.SUCCESS
            data.value = response
        }, { error ->
            _networkState.value = NetworkState.FAILED
            _error.value = error
        })

        return Result(data, _error, _networkState)
    }

    fun createUser(uid: String): Result<Response<User>> {
        val data = MutableLiveData<Response<User>>()

        // TODO: request create user

        return Result(data, _error, _networkState)
    }

    fun getArticles(): Result<PagedList<Article>> {
        val dataSource = ArticleDataSourceFactory()

        val data = LivePagedListBuilder(dataSource, 5).setFetchExecutor(executor).build()

        val error = Transformations.switchMap(dataSource.source) { it.error }

        val networkState = Transformations.switchMap(dataSource.source) { it.networkState }

        return Result(data, error, networkState)
    }
}