package com.agti.pawcare.data

import com.agti.pawcare.data.model.Article
import com.agti.pawcare.data.model.Response
import com.agti.pawcare.data.model.User
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface Services {

    @GET("/artikel")
    fun getArticles(@Query("page") page: String): Call<Response<ArrayList<Article>>>

    @GET("/pengguna/{uid}")
    fun getProfile(@Path("uid") uid: String): Call<Response<User>>

    @PUT("/pengguna/{uid}")
    fun updateProfile(@Path("uid") uid: String): Call<Response<User>>


    companion object {
        private const val BASE_URL = "http://54.169.234.213:8000/v1"
        fun create(): Services {
            val okHttpClient = OkHttpClient().newBuilder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build()
            return Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build().create(Services::class.java)
        }
    }
}