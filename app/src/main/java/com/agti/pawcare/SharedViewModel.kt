package com.agti.pawcare

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.agti.pawcare.data.Repository
import com.agti.pawcare.data.model.Result
import com.agti.pawcare.data.model.User

class SharedViewModel() : ViewModel() {
    val user = MutableLiveData<User>()
}