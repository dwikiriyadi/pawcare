package com.agti.pawcare.ui.petshop

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.agti.pawcare.R

class PetGroomingFragment : Fragment() {

    companion object {
        fun newInstance() = PetGroomingFragment()
    }

    private lateinit var viewModel: PetGroomingViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pet_grooming, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PetGroomingViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
