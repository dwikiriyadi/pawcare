package com.agti.pawcare.ui.profile

import androidx.lifecycle.ViewModel
import com.agti.pawcare.R
import com.agti.pawcare.data.Repository
import com.agti.pawcare.data.model.Item

class ProfileViewModel(repository: Repository) : ViewModel() {

    val items = ArrayList<Item>().apply {
        addAll(listOf(
                Item(title = "Semua Riwayat Medis", image = R.drawable.ic_riwayat),
                Item(title = "Pesanan Obat", image = R.drawable.ic_obat),
                Item(title = "Konsultasi", image = R.drawable.chat),
                Item(title = "Tes Lab", image = R.drawable.ic_test_lab)
        ))
    }
}