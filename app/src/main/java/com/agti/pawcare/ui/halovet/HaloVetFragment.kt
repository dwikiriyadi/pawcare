package com.agti.pawcare.ui.halovet

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.agti.pawcare.R
import com.agti.pawcare.ui.adapter.HaloVetAdapter
import com.agti.pawcare.data.Services
import com.agti.pawcare.data.model.Doctor
import com.agti.pawcare.databinding.FragmentHaloVetBinding
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class HaloVetFragment : androidx.fragment.app.Fragment() {

    private lateinit var binding: FragmentHaloVetBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        activity!!.title = "    Halo Vet"
        setHasOptionsMenu(true)
        (activity as AppCompatActivity?)!!.supportActionBar
        binding = FragmentHaloVetBinding.inflate(inflater, container, false)
        setRecyclerView()
        return binding.root
    }

    private fun setRecyclerView() {
        val items = ArrayList<Doctor>().apply {
            addAll(listOf(
                    Doctor(
                            1, "Drh Maulidiya Wardani", 3, 35000, "20:00", "Univ. Airlangga", "Radhiyan Pet&Care Pulogebang", "082211154368", "doctor2"
                    ),
                    Doctor(
                            2, "Drh Lisa Inamer Hidayati", 3, 35000, "20:00", " Univ. Gadjah Mada", "Radhiyan Pet&Care Pulogebang", "082211154368", "doctor2"
                    ),
                    Doctor(
                            3, "Drh Gita Fatmawati", 3, 35000, "20:00", "Univ. Airlangga", "Radhiyan Pet&Care Condet", "082111712901", "doctor2"
                    ),
                    Doctor(
                            4, "Drh Taskara Danastri", 3, 35000, "20:00", "Univ. Gadjah Mada", "Radhiyan Pet&Care Condet", "082111712901", "doctor2"
                    ),
                    Doctor(
                            5, "Drh Arina Shofiyanies A.", 3, 35000, "20:00", "Univ. Gadjah Mada", "Radhiyan Pet&Care Hankam", "082123882338", "doctor2"
                    )
            ))
        }

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = HaloVetAdapter(items) {
                // TODO: onItemClick
            }
        }
    }
}