package com.agti.pawcare.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.agti.pawcare.data.Repository
import com.agti.pawcare.data.model.Response
import com.agti.pawcare.data.model.Result
import com.agti.pawcare.data.model.User

class EditProfileViewModel(private val repository: Repository) : ViewModel() {

    private val _result = MutableLiveData<Result<Response<User>>>()

    val data = Transformations.switchMap(_result) { it.data }

    val error = Transformations.switchMap(_result) { it.error }

    val networkState = Transformations.switchMap(_result) { it.networkState }

    val name = MutableLiveData<String>()

    val birthday = MutableLiveData<String>()

    val email = MutableLiveData<String>()

    val phone = MutableLiveData<String>()

    val about = MutableLiveData<String>()

    val uid = MutableLiveData<String>()

    fun create() {
        _result.value = uid.value?.let { repository.createUser(it) }
    }

    fun update() {
        _result.value = uid.value?.let { repository.updateUser(it) }
    }
}