package com.agti.pawcare.ui

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.agti.pawcare.MainActivity
import com.agti.pawcare.R
import com.agti.pawcare.databinding.FragmentSplashBinding
import com.agti.pawcare.utility.SharedPrefManager
import com.agti.pawcare.utility.setupLayout

class SplashFragment : Fragment() {

    private val mainActivity by lazy { activity as MainActivity }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity.setupLayout(R.color.colorRed)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding = FragmentSplashBinding.inflate(inflater, container, false)

        Handler().postDelayed({
            var actions = R.id.action_splashFragment_to_homeFragment

            if (!SharedPrefManager.isRegistered) actions = R.id.action_splashFragment_to_editProfileFragment

            if (SharedPrefManager.isSignIn.isNullOrEmpty())
                actions = R.id.action_splashFragment_to_authFragment

            if (SharedPrefManager.isFirstRun) actions = R.id.action_splashFragment_to_introFragment

            findNavController().navigate(actions)
        }, 3000)

        return binding.root
    }
}
