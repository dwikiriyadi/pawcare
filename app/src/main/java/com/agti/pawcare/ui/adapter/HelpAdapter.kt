package com.agti.pawcare.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.agti.pawcare.R
import com.agti.pawcare.data.model.Item
import com.agti.pawcare.databinding.ItemBantuanBinding

class HelpAdapter(private val items: List<Item>, val onItemClick: (item: Item) -> Unit) : RecyclerView.Adapter<BaseViewHolder<Item>>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): BaseViewHolder<Item> = BaseViewHolder(ItemBantuanBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))

    override fun onBindViewHolder(holder: BaseViewHolder<Item>, i: Int) {
        holder.bind(items[i]) {
            onItemClick(items[i])
        }
    }

    override fun getItemCount() = items.size
}