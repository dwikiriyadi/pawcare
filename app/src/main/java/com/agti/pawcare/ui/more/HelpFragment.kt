package com.agti.pawcare.ui.more

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.agti.pawcare.R
import com.agti.pawcare.data.model.Item
import com.agti.pawcare.databinding.LayoutRecyclerViewBinding
import com.agti.pawcare.ui.adapter.HelpAdapter

class HelpFragment : Fragment() {
    var adapter: HelpAdapter? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        activity!!.title = "Bantuan"
        setHasOptionsMenu(true)
        val binding = LayoutRecyclerViewBinding.inflate(inflater, container, false)
        val helpMenu = ArrayList<Item>().apply {
            addAll(listOf<Item>(
                    Item(title = "FAQ", image = R.drawable.faq),
                    Item(title = "Chat dengan layanan konsumen", image = R.drawable.layanan),
                    Item(title = "Email Kami", image = R.drawable.email, isVisible = false),
                    Item(title = "Telepon Kami", image = R.drawable.call, isVisible = false)
            ))
        }

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = HelpAdapter(helpMenu) {
                // TODO: onItemClick
            }
        }
        return binding.root
    }
}