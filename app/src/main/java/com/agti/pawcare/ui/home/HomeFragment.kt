package com.agti.pawcare.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.agti.pawcare.MainActivity
import com.agti.pawcare.R
import com.agti.pawcare.databinding.FragmentHomeBinding
import com.agti.pawcare.ui.adapter.BerandaMenuAdapter
import com.agti.pawcare.utility.setupLayout
import com.agti.pawcare.utility.setupToolbar

class HomeFragment : androidx.fragment.app.Fragment() {

    private val mainActivity by lazy { activity as MainActivity }
    private val model: HomeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity.apply {
            setupLayout(isAppBarVisible = true, isBottomNavigationVisible = true)
            setupToolbar(requireContext(), R.color.colorRed, android.R.color.white)
            supportActionBar?.setIcon(R.drawable.red_logo)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = FragmentHomeBinding.inflate(inflater, container, false)

        binding.menus.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = BerandaMenuAdapter(model.berandaMenu) {
                // TODO: onItemClick
            }
        }

//        binding.articles.apply {
//            layoutManager = LinearLayoutManager(context)
//            adapter = ArticleAdapter() {
//
//            }
//        }

        return binding.root
    }
}