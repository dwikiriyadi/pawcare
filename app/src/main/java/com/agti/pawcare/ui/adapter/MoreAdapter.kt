package com.agti.pawcare.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.agti.pawcare.data.model.Item
import com.agti.pawcare.databinding.ItemMenuLainnyaBinding

class MoreAdapter(private val items: List<Item>, val onItemClick: (item: Item) -> Unit) : RecyclerView.Adapter<BaseViewHolder<Item>>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): BaseViewHolder<Item> = BaseViewHolder(ItemMenuLainnyaBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))

    override fun onBindViewHolder(holder: BaseViewHolder<Item>, i: Int) {
        holder.bind(items[i]) {
            onItemClick(items[i])
        }
    }

    override fun getItemCount() = items.size
}