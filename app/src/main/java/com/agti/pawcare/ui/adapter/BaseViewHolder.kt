package com.agti.pawcare.ui.adapter

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.agti.pawcare.BR

class BaseViewHolder<T>(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(data: T, func: () -> Unit) {
        binding.setVariable(BR.data, data)
        binding.root.setOnClickListener {
            func()
        }
        binding.executePendingBindings()
    }
}