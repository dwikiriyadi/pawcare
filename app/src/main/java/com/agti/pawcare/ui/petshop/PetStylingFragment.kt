package com.agti.pawcare.ui.petshop

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.agti.pawcare.R

class PetStylingFragment : Fragment() {

    companion object {
        fun newInstance() = PetStylingFragment()
    }

    private lateinit var viewModel: PetStylingViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pet_styling, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PetStylingViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
