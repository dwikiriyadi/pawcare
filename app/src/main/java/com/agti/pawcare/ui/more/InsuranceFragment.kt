package com.agti.pawcare.ui.more

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.agti.pawcare.R

/**
 * A simple [Fragment] subclass.
 */
class InsuranceFragment : androidx.fragment.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        activity!!.title = "My Asuransi"
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_insurance, container, false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity!!.finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}