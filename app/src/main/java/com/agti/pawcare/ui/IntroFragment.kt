package com.agti.pawcare.ui

import android.os.Build
import android.os.Bundle
import android.text.Html
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager

import com.agti.pawcare.R
import com.agti.pawcare.databinding.FragmentIntroBinding
import com.agti.pawcare.ui.adapter.IntroAdapter
import com.agti.pawcare.utility.SharedPrefManager

class IntroFragment : Fragment() {
    private lateinit var indicators: ArrayList<TextView>
    private lateinit var layouts: List<Int>
    private lateinit var binding: FragmentIntroBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentIntroBinding.inflate(inflater, container, false)

        layouts = ArrayList<Int>().apply {
            addAll(listOf(R.layout.intro_govet, R.layout.intro_clinic, R.layout.intro_halovet, R.layout.intro_aman))
        }

        addIndicator(0)

        binding.viewPager.apply {
            adapter = IntroAdapter(layoutInflater, layouts)
            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    addIndicator(position)
                }

            })
        }

        binding.start.setOnClickListener {
            SharedPrefManager.isFirstRun = false
            findNavController().navigate(R.id.action_introFragment_to_authFragment)
        }

        return binding.root
    }

    private fun addIndicator(current: Int) {
        indicators = ArrayList()

        binding.indicatorContainer.removeAllViews()
        for ((index, _) in layouts.withIndex()) {
            indicators.add(TextView(context).apply {
                text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml("&#8226;", Html.FROM_HTML_MODE_COMPACT)
                } else {
                    Html.fromHtml("&#8226;")
                }
                textSize = 35.0F
                setTextColor(ContextCompat.getColor(context, R.color.colorInactive))
            })

            binding.indicatorContainer.addView(indicators[index])
        }

        if (indicators.isNotEmpty())
            indicators[current].setTextColor(ContextCompat.getColor(context!!, R.color.colorBackground))
    }
}
