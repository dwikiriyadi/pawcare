package com.agti.pawcare.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.agti.pawcare.data.model.Clinic
import com.agti.pawcare.databinding.ItemPawcareShopBinding

class PetShopAdapter(private val items: List<Clinic>, val onItemClick: (item: Clinic) -> Unit) : androidx.recyclerview.widget.RecyclerView.Adapter<BaseViewHolder<Clinic>>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): BaseViewHolder<Clinic> = BaseViewHolder(ItemPawcareShopBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))

    override fun onBindViewHolder(holder: BaseViewHolder<Clinic>, i: Int) {
        holder.bind(items[i]) {
            onItemClick(items[i])
        }
    }

    override fun getItemCount() = items.size
}