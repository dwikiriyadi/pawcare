package com.agti.pawcare.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.agti.pawcare.R
import com.agti.pawcare.data.model.Doctor
import com.agti.pawcare.databinding.ItemHaloVetBinding

// TODO: Change HaloVetAdapter into Paging adapter

class HaloVetAdapter(private val items: List<Doctor>, val onItemClick: (item: Doctor) -> Unit) : RecyclerView.Adapter<BaseViewHolder<Doctor>>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): BaseViewHolder<Doctor> = BaseViewHolder(ItemHaloVetBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))

    override fun onBindViewHolder(holder: BaseViewHolder<Doctor>, i: Int) {
        holder.bind(items[i]) {
            onItemClick(items[i])
        }
//        holder.tvNamDokter.setText(dataDokter[i].getNamaDokter())
//        holder.tvPengalaman.text = "Pengalaman " + dataDokter[i].getPengalaman().toString() + " Tahun"
//        holder.tvTarif.text = "Rp." + dataDokter[i].getTarif()
//        holder.tvTersedia.text = "Tersedaia sampai " + dataDokter[i].getTersedia().toString() + " WIB"
//        holder.tvPendidiakan.setText(dataDokter[i].getPendidikan())
//        holder.tvKlinik.setText(dataDokter[i].getKlinik())
    }

    override fun getItemCount() = items.size
}