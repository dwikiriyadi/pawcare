package com.agti.pawcare.ui.petshop

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.agti.pawcare.R

class PetDepositFragment : Fragment() {

    companion object {
        fun newInstance() = PetDepositFragment()
    }

    private lateinit var viewModel: PetDepositViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pet_deposit, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PetDepositViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
