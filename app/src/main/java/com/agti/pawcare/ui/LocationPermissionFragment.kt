package com.agti.pawcare.ui

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.navigation.fragment.findNavController
import com.agti.pawcare.MainActivity
import com.agti.pawcare.R
import com.agti.pawcare.databinding.FragmentLocationPermissionBinding

class LocationPermissionFragment : androidx.fragment.app.Fragment() {

    private val mainActivity by lazy { activity as MainActivity }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity.apply {
            binding.toolbar.title = "Aktifkan Lokasi"
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentLocationPermissionBinding.inflate(inflater, container, false)
        binding.active.setOnClickListener {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 200)
        }

        binding.skip.setOnClickListener {
            findNavController().navigate(R.id.action_locationPermissionFragment_to_homeFragment)
        }
        return binding.root
    }
}