package com.agti.pawcare.ui.home

import androidx.lifecycle.ViewModel
import com.agti.pawcare.R
import com.agti.pawcare.data.model.Item

class HomeViewModel : ViewModel() {
    val berandaMenu = ArrayList<Item>().apply {
        addAll(listOf(
                Item(title = "Halo Vet",
                        description = "Tanya Jawab langsung dengan dokter hewan dan klinik hewan terdekat",
                        image = R.drawable.ic_halaovet),
                Item(title = "Go Vet",
                        description = "Layanan keadaan darurat dan home care untuk hewan kesayanganmu",
                        image = R.drawable.ic_govetmenu),
                Item(title = "Petshop & Care",
                        description = "Beli pakan hewan dan perlengkapan hewan langsung diantar dalam waktu 60 menit",
                        image = R.drawable.ic_rpetshop),
                Item(title = "Adopsi",
                        description = "Temukan Teman Peliharaan kesayanganmu di sekitar",
                        image = R.drawable.ic_community)
        ))
    }

    fun getArticles() {

    }
}