package com.agti.pawcare.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.agti.pawcare.MainActivity
import com.agti.pawcare.R
import com.agti.pawcare.databinding.FragmentAuthBinding
import com.agti.pawcare.utility.setupLayout
import com.agti.pawcare.utility.setupToolbar

class AuthFragment : androidx.fragment.app.Fragment() {
    private lateinit var binding: FragmentAuthBinding

    private val mainActivity by lazy { activity as MainActivity }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity.apply {
            setupLayout(isAppBarVisible = true)
            setupToolbar(requireContext(), R.color.colorGrey, R.color.colorRed)
            binding.toolbar.title = resources.getString(R.string.start)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentAuthBinding.inflate(inflater, container, false)
        binding.next.setOnClickListener {
            if (binding.ponselInputLayout.editText?.text.toString().first() != '0')
                phoneVerification("+62${binding.ponsel.text}")
            else binding.ponselInputLayout.error = "No ponsel tidak valid"
        }
        return binding.root
    }

    private fun phoneVerification(phoneNumber: String) {
        when {
            phoneNumber.isEmpty() -> {
                binding.ponselInputLayout.error = "No Ponsel Tidak Boleh Kosong"
                return
            }
            phoneNumber.length < 10 -> {
                binding.ponselInputLayout.error = "No Ponsel tidak valid"
                return
            }
            else -> {
                findNavController().navigate(R.id.action_authFragment_to_verificationFragment, bundleOf(Pair("phone_number", phoneNumber)))
            }
        }
    }
}