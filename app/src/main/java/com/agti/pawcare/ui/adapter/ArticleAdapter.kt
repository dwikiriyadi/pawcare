package com.agti.pawcare.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.agti.pawcare.data.model.Article
import com.agti.pawcare.databinding.ItemArticleBinding

class ArticleAdapter(private val items: List<Article>, val onItemClick: (item: Article) -> Unit) : RecyclerView.Adapter<BaseViewHolder<Article>>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): BaseViewHolder<Article> = BaseViewHolder(ItemArticleBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false))

    override fun onBindViewHolder(holder: BaseViewHolder<Article>, i: Int) {
        holder.bind(items[i]) {
            onItemClick(items[i])
        }
    }

    override fun getItemCount() = items.size
}