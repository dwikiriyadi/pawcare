package com.agti.pawcare.ui.more

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.agti.pawcare.R
import com.agti.pawcare.data.model.Item
import com.agti.pawcare.databinding.LayoutRecyclerViewBinding
import com.agti.pawcare.ui.adapter.MoreAdapter

class MoreFragment : androidx.fragment.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        activity!!.title = "LAINNYA"
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDisplayShowHomeEnabled(false)
        val binding = LayoutRecyclerViewBinding.inflate(inflater, container, false)
        val moreMenu = ArrayList<Item>().apply {
            addAll(listOf(
                    Item(title = "Radhiyan Pet Cash", image = R.drawable.paw_cash),
                    Item(title = "Sambungkan Asuransi", image = R.drawable.asuransi),
                    Item(title = "Bantuan", image = R.drawable.bantuan),
                    Item(title = "Syarat & Ketentuan", image = R.drawable.syarat),
                    Item(title = "Beritahu Teman", image = R.drawable.bagikan, isVisible = false),
                    Item(title = "Sign Out", image = R.drawable.logout, isVisible = false)
            ))
        }

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = MoreAdapter(moreMenu) {
                // TODO: onItemOnClick
            }
        }
        return binding.root
    }
}