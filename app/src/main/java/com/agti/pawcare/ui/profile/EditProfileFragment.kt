package com.agti.pawcare.ui.profile

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.agti.pawcare.MainActivity
import com.agti.pawcare.R
import com.agti.pawcare.SharedViewModel
import com.agti.pawcare.databinding.FragmentEditProfileBinding
import com.agti.pawcare.utility.Injection
import com.agti.pawcare.utility.SharedPrefManager
import com.agti.pawcare.utility.toast

class EditProfileFragment : androidx.fragment.app.Fragment() {

    private val mainActivity by lazy { activity as MainActivity }
    private val model: EditProfileViewModel by viewModels { Injection.provideEditProfileViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity.apply {
            binding.toolbar.title = if (SharedPrefManager.isRegistered) "Edit Profile" else "Lengkapi Profil Kamu"
        }

        arguments?.let {
            model.uid.value = it.getString("uid")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = FragmentEditProfileBinding.inflate(inflater, container, false)

        binding.save.setOnClickListener {
            if (SharedPrefManager.isRegistered) {
                model.update()
            } else model.create()
        }

        model.data.observe(viewLifecycleOwner, Observer { data ->
            requireContext().toast(data.message)
            if (!SharedPrefManager.isRegistered)  {
                var actions: Int = R.id.action_editProfileFragment_to_homeFragment

                if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    actions = R.id.action_editProfileFragment_to_locationPermissionFragment
                SharedPrefManager.isRegistered = true
                findNavController().navigate(actions)
            } else {
                requireContext().toast(data.message)

                findNavController().navigateUp()
            }
        })

        model.error.observe(viewLifecycleOwner, Observer { requireContext().toast(it) })

        model.networkState.observe(viewLifecycleOwner, Observer {
            // TODO: show progress indicator
        })

        return binding.root
    }
}