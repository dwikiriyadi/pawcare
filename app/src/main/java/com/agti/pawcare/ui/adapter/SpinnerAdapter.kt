package com.agti.pawcare.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.agti.pawcare.R
import com.agti.pawcare.data.model.Item

class SpinnerAdapter(private val items: List<Item>) : BaseAdapter() {
    override fun getCount() = items.size

    override fun getItem(position: Int) = items[position]

    override fun getItemId(position: Int) = position.toLong()

    override fun getView(i: Int, view: View, parent: ViewGroup): View {
        val item = items[i]
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_riwayat, null)
        view.findViewById<ImageView>(R.id.image).setImageResource(item.image as Int)
        view.findViewById<TextView>(R.id.title).text = item.title
        return view
    }

}