package com.agti.pawcare.ui.govet

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.agti.pawcare.R

/**
 * A simple [Fragment] subclass.
 */
class GoVetFragment : androidx.fragment.app.Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        (activity as AppCompatActivity?)!!.supportActionBar
        activity!!.title = "    Go-Vet"
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_go_vet, container, false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity!!.finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}