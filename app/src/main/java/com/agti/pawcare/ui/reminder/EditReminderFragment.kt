package com.agti.pawcare.ui.reminder

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.agti.pawcare.R

class EditReminderFragment : Fragment() {

    companion object {
        fun newInstance() = EditReminderFragment()
    }

    private lateinit var viewModel: EditReminderViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_edit_reminder, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(EditReminderViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
