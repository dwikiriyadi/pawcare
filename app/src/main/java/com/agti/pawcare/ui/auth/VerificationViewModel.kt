package com.agti.pawcare.ui.auth

import android.os.CountDownTimer
import android.text.format.DateUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.agti.pawcare.data.Repository
import com.agti.pawcare.data.model.Response
import com.agti.pawcare.data.model.Result
import com.agti.pawcare.data.model.User
import com.google.firebase.auth.PhoneAuthProvider

class VerificationViewModel(private val repository: Repository) : ViewModel() {
    private lateinit var timer: CountDownTimer

    private val _result = MutableLiveData<Result<Response<User>>>()

    val data = Transformations.switchMap(_result) { it.data }

    val error = Transformations.switchMap(_result) { it.error }

    val networkState = Transformations.switchMap(_result) { it.networkState }

    private val verificationId = Transformations.map(_result) { it.extras?.getString("verificationId") }

    val phoneNumber = MutableLiveData<String>()

    val otpCode = MutableLiveData<String>()

    private val _isTimerFinished = MutableLiveData<Boolean>()
    val isTimerFinished: LiveData<Boolean>
        get() = _isTimerFinished

    private val _currentTime = MutableLiveData<Long>()
    private val currentTime: LiveData<Long>
        get() = _currentTime

    val currentTimeString = Transformations.map(currentTime) { time ->
        DateUtils.formatElapsedTime(time)
    }

    companion object {

        private const val DONE = 0L

        private const val ONE_SECOND = 1000L

        private const val COUNTDOWN_TIME = 30000L
    }

    private fun setTimer() {
        _isTimerFinished.value = false
        timer = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {
            override fun onFinish() {
                _currentTime.value = DONE
                onTimerFinish()
            }

            override fun onTick(millisUntilFinished: Long) {
                _currentTime.value = millisUntilFinished / ONE_SECOND
            }

        }

        timer.start()
    }

    private fun onTimerFinish() {
        _isTimerFinished.value = true
    }

    fun requestOTPCode() {
        _result.value = repository.signInWithPhoneNumber(phoneNumber.value!!, COUNTDOWN_TIME / ONE_SECOND)
        setTimer()
    }

    fun verification() {
        val credential = PhoneAuthProvider.getCredential(verificationId.value!!, otpCode.value!!)
        _result.value = repository.signInWithCredential(credential)
    }

    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }
}