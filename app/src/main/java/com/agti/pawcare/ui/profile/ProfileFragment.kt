package com.agti.pawcare.ui.profile

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.agti.pawcare.MainActivity
import com.agti.pawcare.R
import com.agti.pawcare.ui.adapter.SpinnerAdapter
import com.agti.pawcare.data.model.Item
import com.agti.pawcare.databinding.FragmentProfileBinding
import com.agti.pawcare.utility.Injection
import com.agti.pawcare.utility.setupLayout
import com.agti.pawcare.utility.setupToolbar

class ProfileFragment : androidx.fragment.app.Fragment() {

    private val mainActivity by lazy { activity as MainActivity }
    private val model : ProfileViewModel by viewModels { Injection.provideProfileViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity.apply {
            setupLayout(isAppBarVisible = true, isBottomNavigationVisible = true)
            setupToolbar(requireContext(), R.color.colorRed, android.R.color.white)
            binding.toolbar.title = "Profil"
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentProfileBinding.inflate(inflater, container, false)
        
        return binding.root
    }
}