package com.agti.pawcare.ui.auth

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.agti.pawcare.MainActivity
import com.agti.pawcare.R
import com.agti.pawcare.SharedViewModel
import com.agti.pawcare.databinding.FragmentVerificationBinding
import com.agti.pawcare.utility.Injection
import com.agti.pawcare.utility.SharedPrefManager
import com.agti.pawcare.utility.setupToolbar
import com.agti.pawcare.utility.toast

class VerificationFragment : Fragment() {

    private val mainActivity by lazy { activity as MainActivity }
    private val model: VerificationViewModel by viewModels { Injection.provideVerificationViewModelFactory() }
    private val sharedViewModel: SharedViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivity.apply {
            setupToolbar(requireContext(), R.color.colorGrey, R.color.colorPurple)
            binding.toolbar.title = resources.getString(R.string.verification)
        }

        arguments?.let {
            model.phoneNumber.value = it.getString("phone_number")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = FragmentVerificationBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewmodel = model
        }

        binding.verification.setOnClickListener {
            model.verification()
        }

        binding.resend.setOnClickListener {
            model.requestOTPCode()
        }

        model.requestOTPCode()

        model.networkState.observe(viewLifecycleOwner, Observer {
            // TODO: show loading when request for verification
        })

        model.error.observe(viewLifecycleOwner, Observer {
            if (it == "404") {
                SharedPrefManager.isRegistered = false
                findNavController().navigate(R.id.action_verificationFragment_to_editProfileFragment)
            } else {
                requireContext().toast(it)
            }
        })

        model.data.observe(viewLifecycleOwner, Observer {
            SharedPrefManager.isRegistered = true
            SharedPrefManager.isSignIn = it.data.uid
            sharedViewModel.user.value = it.data
            var actions: Int = R.id.action_verificationFragment_to_homeFragment

            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                actions = R.id.action_verificationFragment_to_locationPermissionFragment

            findNavController().navigate(actions)
        })

        return binding.root
    }
}