package com.agti.pawcare.utility

import com.agti.pawcare.SharedViewModelFactory
import com.agti.pawcare.data.Repository
import com.agti.pawcare.ui.auth.VerificationViewModelFactory
import com.agti.pawcare.ui.profile.EditProfileViewModelFactory
import com.agti.pawcare.ui.profile.ProfileViewModelFactory
import java.util.concurrent.Executors

object Injection {
    private val NETWORK_IO = Executors.newFixedThreadPool(5)

    private fun provideRepository() = Repository(NETWORK_IO)

    fun provideSharedViewModelFactory(): SharedViewModelFactory = SharedViewModelFactory(provideRepository())

    fun provideVerificationViewModelFactory(): VerificationViewModelFactory = VerificationViewModelFactory(provideRepository())

    fun provideProfileViewModelFactory(): ProfileViewModelFactory = ProfileViewModelFactory(provideRepository())

    fun provideEditProfileViewModelFactory(): EditProfileViewModelFactory = EditProfileViewModelFactory(provideRepository())
}
