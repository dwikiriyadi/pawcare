package com.agti.pawcare.utility

import android.content.Context
import android.content.SharedPreferences

object SharedPrefManager {
    private const val PREF_NAME = "pawcare"
    private lateinit var sharedPreferences: SharedPreferences

    private val IS_FIRST_RUN = Pair("is_first_run", true)
    private val IS_SIGN_IN = Pair("is_sign_in", null)
    private val IS_REGISTERED = Pair("is_registered", false)
    private val IS_LOCATION_PERMITTED = Pair("is_location_permitted", false)

    fun init(context: Context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    private inline fun SharedPreferences.edit(operations: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operations(editor)
        editor.apply()
    }

    var isFirstRun: Boolean
        get() = sharedPreferences.getBoolean(IS_FIRST_RUN.first, IS_FIRST_RUN.second)
        set(value) = sharedPreferences.edit {
            it.putBoolean(IS_FIRST_RUN.first, value)
        }

    var isSignIn: String?
        get() = sharedPreferences.getString(IS_SIGN_IN.first, IS_SIGN_IN.second)
        set(value) = sharedPreferences.edit {
            it.putString(IS_SIGN_IN.first, value)
        }

    var isRegistered: Boolean
        get() = sharedPreferences.getBoolean(IS_REGISTERED.first, IS_REGISTERED.second)
        set(value) = sharedPreferences.edit {
            it.putBoolean(IS_REGISTERED.first, value)
        }

    var isLocationPermitted: Boolean
        get() = sharedPreferences.getBoolean(IS_LOCATION_PERMITTED.first, IS_LOCATION_PERMITTED.second)
        set(value) = sharedPreferences.edit {
            it.putBoolean(IS_LOCATION_PERMITTED.first, value)
        }
}