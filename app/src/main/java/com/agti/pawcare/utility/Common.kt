package com.agti.pawcare.utility

import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.agti.pawcare.MainActivity
import com.agti.pawcare.R
import com.bumptech.glide.Glide
import java.text.DecimalFormat
import kotlin.math.roundToInt


fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.toastLong(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

@BindingAdapter("android:image_source")
fun ImageView.setImageSource(source: Any) {
    if (source is String)
        Glide.with(this.context).load(source).centerCrop().into(this)
    else {
        this.setImageResource(source as Int)
    }
}

fun toDensityPixel(value: Int) = (value * Resources.getSystem().displayMetrics.density).roundToInt()

fun MainActivity.setupLayout(statusBarColor: Int = R.color.colorPrimaryDark, isAppBarVisible: Boolean = false, isBottomNavigationVisible: Boolean = false) {
    this.window?.statusBarColor = ContextCompat.getColor(this, statusBarColor)
    this.binding.appbar.visibility = if (isAppBarVisible) View.VISIBLE else View.GONE
    this.binding.bottomNavigation.visibility = if (isBottomNavigationVisible) View.VISIBLE else View.GONE
}

fun MainActivity.setupToolbar(context: Context, backgroundColor: Int, textColor: Int) {
    this.binding.appbar.setBackgroundColor(ContextCompat.getColor(context, backgroundColor))
    this.binding.toolbar.setTitleTextColor(ContextCompat.getColor(context, textColor))
}

fun Int.moneyFormat(): String = DecimalFormat("###,###,###").format(this)
